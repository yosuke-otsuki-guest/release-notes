# Translation of old-stuff.po to Galician
# Copyright (C) 2021 Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# Pablo <parodper@gmail.com>, 2021.
#
# Traductores:
#     Para esta traducción usei (Pablo) o dicionario do Proxecto Trasno (http://termos.trasno.gal/) e o DiGaTIC (http://www.digatic.org/gl)
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 11\n"
"POT-Creation-Date: 2021-04-05 11:25+0200\n"
"PO-Revision-Date: 2021-03-17 19:00+0100\n"
"Last-Translator: Pablo <parodper@gmail.com>\n"
"Language-Team: Galician <debian-l10n-galician@lists.debian.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"

#. type: Attribute 'lang' of: <appendix>
#: en/old-stuff.dbk:8
msgid "en"
msgstr "gl"

#. type: Content of: <appendix><title>
#: en/old-stuff.dbk:9
msgid "Managing your &oldreleasename; system before the upgrade"
msgstr "Xestionar o teu sistema &oldreleasename; antes de actualizar"

#. type: Content of: <appendix><para>
#: en/old-stuff.dbk:11
msgid ""
"This appendix contains information on how to make sure you can install or "
"upgrade &oldreleasename; packages before you upgrade to &releasename;.  This "
"should only be necessary in specific situations."
msgstr ""
"Este apéndice contén información sobre como asegurarse de que podes instalar "
"ou actualizar paquetes &oldreleasename; antes de actualizar a "
"&releasename;.  Esto só debería ser necesario en contadas situacións."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:16
msgid "Upgrading your &oldreleasename; system"
msgstr "Actualizando o teu sistema &oldreleasename;"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:18
msgid ""
"Basically this is no different from any other upgrade of &oldreleasename; "
"you've been doing.  The only difference is that you first need to make sure "
"your package list still contains references to &oldreleasename; as explained "
"in <xref linkend=\"old-sources\"/>."
msgstr ""
"Isto non é distinto de calquera outra actualización de &oldreleasename; que "
"xa fixeras.  A única diferenza é que primeiro necesitas asegurarte de que a "
"túa lista de paquetes inda contén referencias a &oldreleasename;, tal coma "
"se explica en <xref linkend=\"old-sources\"/>."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:24
msgid ""
"If you upgrade your system using a Debian mirror, it will automatically be "
"upgraded to the latest &oldreleasename; point release."
msgstr ""
"Se actualizas o teu sistema usando un servidor espello, el mesmo xa se "
"actualizará automaticamente á última versión maior &oldreleasename;."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:30
msgid "Checking your APT source-list files"
msgstr "Comproba os teus ficheiros de fontes APT"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:32
msgid ""
"If any of the lines in your APT source-list files (see <ulink url=\"&url-"
"man;/&releasename;/apt/sources.list.5.html\">sources.list(5)</ulink>)  "
"contain references to <quote><literal>stable</literal></quote>, this is "
"effectively pointing to &releasename; already. This might not be what you "
"want if you are not yet ready for the upgrade.  If you have already run "
"<command>apt update</command>, you can still get back without problems by "
"following the procedure below."
msgstr ""
"Se calquera das liñas nos teus ficheiros de fontes APT (véxase <ulink url="
"\"&url-man;/&releasename;/apt/sources.list.5.html\">sources.list(5)</ulink>) "
"contén referencias a <quote><literal>stable</literal></quote>, é que xa "
"apunta a &releasename;. Isto pode non ser o que ti queres se non estás listo "
"para a actualización. Se xa executaches <command>apt update</command> inda "
"podes volverte sen problemas seguindo os pasos seguintes."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:40
msgid ""
"If you have also already installed packages from &releasename;, there "
"probably is not much point in installing packages from &oldreleasename; "
"anymore.  In that case you will have to decide for yourself whether you want "
"to continue or not.  It is possible to downgrade packages, but that is not "
"covered here."
msgstr ""
"Se tamén instalaches paquetes dende &releasename; xa non ten sentido que "
"instales paquetes dende &oldreleasename;.  Nese caso tes que decidir ti se "
"queres continuar ou non.  É posible volver a unha versión anterior dos "
"paquetes, pero iso non se trata neste documento."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:46
msgid ""
"As root, open the relevant APT source-list file (such as <filename>/etc/apt/"
"sources.list</filename>) with your favorite editor, and check all lines "
"beginning with <literal>deb http:</literal>, <literal>deb https:</literal>, "
"<literal>deb tor+http:</literal>, <literal>deb tor+https:</literal>, "
"<literal>URIs: http:</literal>, <literal>URIs: https:</literal>, "
"<literal>URIs: tor+http:</literal> or <literal>URIs: tor+https:</literal> "
"for a reference to <quote><literal>stable</literal></quote>.  If you find "
"any, change <literal>stable</literal> to <literal>&oldreleasename;</literal>."
msgstr ""
"Como superusuario, abre o ficheiro de fontes de APT necesario (por exemplo "
"<filename>/etc/apt/sources.list</filename>) co teu editor de texto favorito "
"e comproba si todas as liñas que comecen con <literal>deb http:</literal>, "
"<literal>deb https:</literal>, <literal>deb tor+http:</literal>, "
"<literal>deb tor+https:</literal>, <literal>URIs: http:</literal>, "
"<literal>URIs: https:</literal>, <literal>URIs: tor+http:</literal> ou "
"<literal>URIs: tor+https:</literal> fan referencia a <quote><literal>stable</"
"literal></quote>.  Se atopas algunha cambia <literal>stable</literal> por "
"<literal>&oldreleasename;</literal>."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:57
msgid ""
"If you have any lines starting with <literal>deb file:</literal> or "
"<literal>URIs: file:</literal>, you will have to check for yourself if the "
"location they refer to contains a &oldreleasename; or &releasename; archive."
msgstr ""
"Se tes algunha liña que comece por <literal>deb file:</literal> ou "
"<literal>URIs: file:</literal> deberás comprobar ti se a localización á que "
"se refiren contén un arquivo &oldreleasename; ou &releasename;."

#. type: Content of: <appendix><section><important><para>
#: en/old-stuff.dbk:64
msgid ""
"Do not change any lines that begin with <literal>deb cdrom:</literal> or "
"<literal>URIs: cdrom:</literal>.  Doing so would invalidate the line and you "
"would have to run <command>apt-cdrom</command> again.  Do not be alarmed if "
"a <literal>cdrom:</literal> source line refers to <quote><literal>unstable</"
"literal></quote>.  Although confusing, this is normal."
msgstr ""
"Non cambies ningunha liña que comece por <literal>deb cdrom:</literal> ou "
"<literal>URIs: cdrom:</literal>.  Facer iso invalidaría a liña e terías que "
"executar de novo <command>apt-cdrom</command>.  Non te asustes se unha liña "
"<literal>cdrom:</literal> fai referencia a <quote><literal>unstable</"
"literal></quote>.  Inda que pode ser confuso, isto é normal."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:73
msgid "If you've made any changes, save the file and execute"
msgstr "Se fixeches algún cambio, garda o ficheiro e executa"

#. type: Content of: <appendix><section><screen>
#: en/old-stuff.dbk:76
#, no-wrap
msgid "# apt update\n"
msgstr "# apt update\n"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:79
msgid "to refresh the package list."
msgstr "para actualizar a lista de paquetes."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:84
msgid "Removing obsolete configuration files"
msgstr "Eliminar ficheiros de configuración obsoletos"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:86
msgid ""
"Before upgrading your system to &releasename;, it is recommended to remove "
"old configuration files (such as <filename>*.dpkg-{new,old}</filename> files "
"under <filename>/etc</filename>) from the system."
msgstr ""
"Antes de actualizar o teu sistema a &releasename;, recoméndanse borrar os "
"ficheiros de configuración vellos (por exemplo os ficheiros <filename>*.dpkg-"
"{new,old}</filename> en <filename>/etc</filename>) do sistema."

#~ msgid "Upgrade legacy locales to UTF-8"
#~ msgstr "Actualizar localizacións vellas a UTF-8"

#~ msgid ""
#~ "Using a legacy non-UTF-8 locale has been unsupported by desktops and "
#~ "other mainstream software projects for a long time. Such locales should "
#~ "be upgraded by running <command>dpkg-reconfigure locales</command> and "
#~ "selecting a UTF-8 default. You should also ensure that users are not "
#~ "overriding the default to use a legacy locale in their environment."
#~ msgstr ""
#~ "Hai tempo que os escritorios e outros proxectos importantes só admiten "
#~ "localizacións en UTF-8. Esas localizacións vellas deben actualizarse "
#~ "executando <command>dpkg-reconfigure locales</command> e seleccionando "
#~ "UTF-8 como defecto. Tamén deberías asegurarte de que os usuarios non "
#~ "están a sobrescribir o valor por defecto para usar localizacións antigas "
#~ "no seu ambiente."
