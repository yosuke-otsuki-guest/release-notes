# Copyright (C) 2020 Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# galaxico <galas@tee.gr>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2021-05-04 19:18+0200\n"
"PO-Revision-Date: 2020-09-08 17:55+0300\n"
"Last-Translator: galaxico <galas@tee.gr>\n"
"Language-Team: debian-l10n-greek@lists.debian.org\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.3\n"

#. type: Attribute 'lang' of: <chapter>
#: en/about.dbk:8
msgid "en"
msgstr "el"

#. type: Content of: <chapter><title>
#: en/about.dbk:9
msgid "Introduction"
msgstr "Εισαγωγή"

#. type: Content of: <chapter><para>
#: en/about.dbk:11
msgid ""
"This document informs users of the &debian; distribution about major changes "
"in version &release; (codenamed &releasename;)."
msgstr ""
"Το παρόν κείμενο πληροφορεί τους χρήστες της διανομής &debian; σχετικά με "
"μείζονες αλλαγές στην έκδοση &release; (με την κωδική ονομασία "
"&releasename;)."

#. type: Content of: <chapter><para>
#: en/about.dbk:15
msgid ""
"The release notes provide information on how to upgrade safely from release "
"&oldrelease; (codenamed &oldreleasename;) to the current release and inform "
"users of known potential issues they could encounter in that process."
msgstr ""
"Οι Σημειώσεις της Έκδοσης παρέχουν πληροφορίες για το πώς να κάνετε με "
"ασφάλεια την αναβάθμιση από την έκδοση &oldrelease; (με κωδική ονομασία "
"&oldreleasename;) στην τρέχουσα έκδοση και πληροφορούν τους χρήστες για "
"γνωστά πιθανά προβλήματα που θα μπορούσαν να συναντήσουν σ' αυτή τη "
"διαδικασία."

#. type: Content of: <chapter><para>
#: en/about.dbk:21
#, fuzzy
#| msgid ""
#| "You can get the most recent version of this document from <ulink url="
#| "\"&url-release-notes;\"></ulink>.  If in doubt, check the date on the "
#| "first page to make sure you are reading a current version."
msgid ""
"You can get the most recent version of this document from <ulink url=\"&url-"
"release-notes;\"></ulink>."
msgstr ""
"Μπορείτε να αποκτήσετε την πιο πρόσφατη έκδοση αυτού του κειμένου από τον "
"σύνδεσμο <ulink url=\"&url-release-notes;\"></ulink>. Αν έχετε αμφιβολίες, "
"ελέγξτε την ημερομηνία στην πρώτη σελίδα για να βεβαιωθείτε ότι διαβάζετε "
"μια τωρινή έκδοση."

#. type: Content of: <chapter><caution><para>
#: en/about.dbk:26
msgid ""
"Note that it is impossible to list every known issue and that therefore a "
"selection has been made based on a combination of the expected prevalence "
"and impact of issues."
msgstr ""
"Σημειώστε ότι είναι αδύνατον να καταγράψουμε κάθε γνωστό πρόβλημα και ότι ως "
"εκ τούτου έχει γίνει μια επιλογή με βάση έναν συνδυασμό της αναμενόμενης "
"συχνότητας και επιπτώσεων των προβλημάτων. "

#. type: Content of: <chapter><para>
#: en/about.dbk:32
msgid ""
"Please note that we only support and document upgrading from the previous "
"release of Debian (in this case, the upgrade from &oldreleasename;).  If you "
"need to upgrade from older releases, we suggest you read previous editions "
"of the release notes and upgrade to &oldreleasename; first."
msgstr ""
"Σημειώστε ότι παρέχουμε υποστήριξη και τεκμηρίωση για την αναβάθμιση μόνο "
"από την αμέσως προηγούμενη έκδοση του Debian (στην παρούσα περίπτωση για την "
"αναβάθμιση από την έκδοση &oldreleasename;). Αν χρειάζεστε να κάνετε "
"αναβάθμιση από παλιότερες εκδόσεις, συνιστούμε να διαβάσετε προηγούμενες "
"εκδόσεις των Σημειώσεων Έκδοσης και να κάνετε αναβάθμιση πρώτα στην έκδοση "
"&oldreleasename;."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:40
msgid "Reporting bugs on this document"
msgstr "Αναφέροντας σφάλματα για το παρόν κείμενο"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:42
msgid ""
"We have attempted to test all the different upgrade steps described in this "
"document and to anticipate all the possible issues our users might encounter."
msgstr ""
"Έχουμε προσπαθήσει να ελέγξουμε όλα τα διαφορετικά βήματα της αναβάθμισης "
"που περιγράφονται στο παρόν κείμενο και να προκαταβάλουμε όλα τα πιθανά "
"προβλήματα που είναι πιθανόν να αντιμετωπίσουν οι χρήστες μας."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:47
msgid ""
"Nevertheless, if you think you have found a bug (incorrect information or "
"information that is missing)  in this documentation, please file a bug in "
"the <ulink url=\"&url-bts;\">bug tracking system</ulink> against the "
"<systemitem role=\"package\">release-notes</systemitem> package. You might "
"first want to review the <ulink url=\"&url-bts-rn;\">existing bug reports</"
"ulink> in case the issue you've found has already been reported. Feel free "
"to add additional information to existing bug reports if you can contribute "
"content for this document."
msgstr ""
"Παρ' όλα αυτά, αν νομίζετε ότι έχετε βρει κάποιο σφάλμα (λανθασμένη "
"πληροφορία ή πληροφορία που λείπει) στην παρούσα τεκμηρίωση, παρακαλούμε "
"υποβάλετε μια αναφορά σφάλματος στο <ulink url=\"&url-bts;\">Σύστημα "
"Ινχηλάτησης Σφαλμάτων</ulink> σε σχέση με το πακέτο <systemitem role="
"\"package\">release-notes</systemitem>. Πιθανόν να θέλετε προηγουμένως να "
"δείτε τις <ulink url=\"&url-bts-rn;\">υπάρχουσες αναφορές σφαλμάτων</ulink> "
"για την περίπτωση που το πρόβλημα που έχετε βρει έχει ήδη αναφερθεί. Νιώστε "
"ελεύθεροι/ες να προσθέσετε επιπλέον πληροφορίες σε υπάρχουσες αναφορές "
"σφαλμάτων αν μπορείτε να συνεισφέρετε περιεχόμενο στο παρόν κείμενο."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:59
msgid ""
"We appreciate, and encourage, reports providing patches to the document's "
"sources. You will find more information describing how to obtain the sources "
"of this document in <xref linkend=\"sources\"/>."
msgstr ""
"Εκτιμούμε και ενθαρρύνουμε αναφορές που παρέχουν επιδιoρθώσεις (patches) "
"στον πηγαίο κώδικα αυτού του κειμένου. Θα βρείτε περισσότερες πληροφορίες "
"για το πώς να αποκτήσετε τον πηγαίο κώδικα αυτού του κειμένου στην ενότητα "
"<xref linkend=\"sources\"/>."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:67
msgid "Contributing upgrade reports"
msgstr "Συνεισφέρετε αναφορές αναβάθμισης"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:69
msgid ""
"We welcome any information from users related to upgrades from "
"&oldreleasename; to &releasename;.  If you are willing to share information "
"please file a bug in the <ulink url=\"&url-bts;\">bug tracking system</"
"ulink> against the <systemitem role=\"package\">upgrade-reports</systemitem> "
"package with your results.  We request that you compress any attachments "
"that are included (using <command>gzip</command>)."
msgstr ""
"Είναι ευπρόσδεκτη κάθε πληροφορία από χρήστες που σχετίζεται με αναβαθμίσεις "
"από την έκδοση &oldreleasename; στην έκδοση &releasename;.  Αν προτίθεστε να "
"μοιραστείτε τέτοιες πληροφορίες παρακαλούμε υποβάλετε μια αναφορά σφάλματος "
"στο <ulink url=\"&url-bts;\">Σύστημα Παρακολούθησης Σφαλμάτων</ulink> σε "
"σχέση με το πακέτο <systemitem role=\"package\">upgrade-reports</systemitem> "
"με τα αποτελέσματά σας. Απαιτούμε να συμπιέσετε όποια συνημμένα "
"περιλαμβάνονται (με τη χρήση του <command>gzip</command>)."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:78
msgid ""
"Please include the following information when submitting your upgrade report:"
msgstr ""
"Παρακαλούμε συμπεριλάβετε τις ακόλουθες πληροφορίες όταν υποβάλλετε την "
"αναφορά αναβάθμισης:"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:85
msgid ""
"The status of your package database before and after the upgrade: "
"<systemitem role=\"package\">dpkg</systemitem>'s status database available "
"at <filename>/var/lib/dpkg/status</filename> and <systemitem role=\"package"
"\">apt</systemitem>'s package state information, available at <filename>/var/"
"lib/apt/extended_states</filename>.  You should have made a backup before "
"the upgrade as described at <xref linkend=\"data-backup\"/>, but you can "
"also find backups of <filename>/var/lib/dpkg/status</filename> in <filename>/"
"var/backups</filename>."
msgstr ""
"Την κατάσταση της βάσης δεδομένων των πακέτων σας πριν και μετά την "
"αναβάθμιση: την βάση της κατάστασης του <systemitem role=\"package\">dpkg</"
"systemitem>, που είναι διαθέσιμη στο αρχείο <filename>/var/lib/dpkg/status</"
"filename> και τις πληροφορίες της κατάστασης των πακέτων του <systemitem "
"role=\"package\">apt</systemitem>, που είναι διαθέσιμη στο αρχείο <filename>/"
"var/lib/apt/extended_states</filename>. Θα πρέπει να έχετε πάρει αντίγραφα "
"ασφαλείας των αρχείων πριν από την αναβάθμιση όπως περιγράφεται στην ενότητα "
"<xref linkend=\"data-backup\"/>, αλλά μπορείτε να βρείτε αντίγραφα ασφαλείας "
"του αρχείου <filename>/var/lib/dpkg/status</filename> στον κατάλογο "
"<filename>/var/backups</filename>."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:98
msgid ""
"Session logs created using <command>script</command>, as described in <xref "
"linkend=\"record-session\"/>."
msgstr ""
"Αρχεία καταγραφης της συνεδρίας χρησιμοποιώντας <command>script</command>, "
"όπως περιγράφεται στην ενότητα <xref linkend=\"record-session\"/>."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:104
msgid ""
"Your <systemitem role=\"package\">apt</systemitem> logs, available at "
"<filename>/var/log/apt/term.log</filename>, or your <command>aptitude</"
"command> logs, available at <filename>/var/log/aptitude</filename>."
msgstr ""
"Τα αρχεία καταγραφής του <systemitem role=\"package\">apt</systemitem>, που "
"είναι διαθέσιμα στο αρχείο <filename>/var/log/apt/term.log</filename>, ή τα "
"αρχεία καταγραφής του <command>aptitude</command>, που είναι διαθέσιμα στον "
"κατάλογο <filename>/var/log/aptitude</filename>."

#. type: Content of: <chapter><section><note><para>
#: en/about.dbk:113
msgid ""
"You should take some time to review and remove any sensitive and/or "
"confidential information from the logs before including them in a bug report "
"as the information will be published in a public database."
msgstr ""
"Θα πρέπει να διαθέσετε λίγο χρόνο να επιθεωρήσετε και να αφαιρέσετε "
"οποιαδήποτε ευαίσθητη και/ή εμπιστευτική πληροφορία από τα αρχεία καταγραφής "
"πριν τα συμπεριλάβετε σε μια αναφορά σφάλματος καθώς αυτή η πληροφορία θα "
"δημοσιευτεί σε μια δημόσια βάση δεδομένων."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:122
msgid "Sources for this document"
msgstr "Πηγές για το παρόν κείμενο"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:124
msgid ""
"The source of this document is in DocBook XML<indexterm><primary>DocBook "
"XML</primary></indexterm> format. The HTML version is generated using "
"<systemitem role=\"package\">docbook-xsl</systemitem> and <systemitem role="
"\"package\">xsltproc</systemitem>. The PDF version is generated using "
"<systemitem role=\"package\">dblatex</systemitem> or <systemitem role="
"\"package\">xmlroff</systemitem>. Sources for the Release Notes are "
"available in the Git repository of the <emphasis>Debian Documentation "
"Project</emphasis>.  You can use the <ulink url=\"&url-vcs-release-notes;"
"\">web interface</ulink> to access its files individually through the web "
"and see their changes.  For more information on how to access Git please "
"consult the <ulink url=\"&url-ddp-vcs-info;\">Debian Documentation Project "
"VCS information pages</ulink>."
msgstr ""
"Ο πηγαίος κώδικας για το παρόν κείμενο είναι στον μορφότυπο DocBook "
"XML<indexterm><primary>DocBook XML</primary></indexterm>. Η HTML έκδοση "
"παράγεται με την χρήση των πακέτων<systemitem role=\"package\">docbook-xsl</"
"systemitem> και <systemitem role=\"package\">xsltproc</systemitem>. Η PDF "
"έκδοση παράγεται με τη χρήση του<systemitem role=\"package\">dblatex</"
"systemitem> ή του <systemitem role=\"package\">xmlroff</systemitem>. Οι "
"πηγές για τις Σημειώσεις της Έκδοσης είναι διαθέσιμες στο Git αποθετήριο του "
"<emphasis>Σχεδίου Τεκμηρίωσης του Debian</emphasis>. Μπορείτε να "
"χρησιμοποιήσετε τη <ulink url=\"&url-vcs-release-notes;\">web διεπαφή</"
"ulink> για να έχετε πρόσβαση στα μεμονωμένα αρχεία τους μέσω του web και να "
"δείτε τις αλλαγές τους. Για περισσότερες πληροφορίες σχετικά με την πρόσβαση "
"στο Git παρακαλούμε συμβουλευτείτε τις σελίδες στο <ulink url=\"&url-ddp-vcs-"
"info;\">Debian Documentation Project VCS information pages</ulink>."
